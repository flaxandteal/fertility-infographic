import Vue from 'vue'
import Vuex from 'vuex'
import { spatialsankey } from 'd3-spatialsankey'
import * as d3base from 'd3'
const d3 = Object.assign(d3base, { spatialsankey })

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    year: 2018,
    ste: "",
    isSelected: false,
    nodes: {},
    links: [],
    codes: null,
    filter: {},
    articles: {},
    states: {}
  },
  mutations: {
    setFilter (state, filter) {
      state.filter = filter
    },
    setStates (state, states) {
      state.states = states
    },
    setSte (state, ste) {
      state.ste = ste
    },
    setIsSelected (state, isSelected) {
      state.isSelected = isSelected
    },
    setData(state, { nodes, links }) {
      state.nodes = nodes
      state.links = links
    },
    setCodes(state, codes) {
      state.codes = codes
    },
    setArticles(state, articles) {
      state.articles = articles;
    }
  },
  actions: {
    loadArticles: function ({ commit }) {
        return commit('setArticles', {
          "IE" : [
            {
              "Title": "Ireland’s birth rate down 25% compared to Celtic Tiger period",
              "Image": "https://www.irishtimes.com/polopoly_fs/1.4055231.1571413030!/image/image.jpg_gen/derivatives/box_620_330/image.jpg",
              "URL": "https://www.irishtimes.com/news/ireland/irish-news/ireland-s-birth-rate-down-25-compared-to-celtic-tiger-period-1.4055232"
            },
            {
              "Title": "This is the Irish county with the highest fertility rate",
              "Image": "https://img.maximummedia.ie/herfamily_ie/eyJkYXRhIjoie1widXJsXCI6XCJodHRwOlxcXC9cXFwvbWVkaWEtaGVyZmFtaWx5Lm1heGltdW1tZWRpYS5pZS5zMy5hbWF6b25hd3MuY29tXFxcL3dwLWNvbnRlbnRcXFwvdXBsb2Fkc1xcXC8yMDE5XFxcLzAyXFxcLzI2MTUzNjU0XFxcL2lTdG9jay01MjA2MjEwMDguanBnXCIsXCJ3aWR0aFwiOjY0MCxcImhlaWdodFwiOjM2MCxcImRlZmF1bHRcIjpcImh0dHBzOlxcXC9cXFwvd3d3LmhlcmZhbWlseS5pZVxcXC9hc3NldHNcXFwvaW1hZ2VzXFxcL2hlcmZhbWlseVxcXC9uby1pbWFnZS5wbmc_aWQ9NjQ1NzMzYTRmNzBiZTVkM2MzNGNcIixcIm9wdGlvbnNcIjpbXX0iLCJoYXNoIjoiYzBhNzQ3MTNiN2FlMTJmNmI2M2I5MTE4YTkyYjA2YjExYTkzYjI1MSJ9/istock-520621008.jpg",
              "URL": "https://www.herfamily.ie/fertility/longford-county-highest-fertility-rate-ireland-342168"
            }],
          "IE-U" : [
          {
            "Title": "Data: An in-depth look at Ulster's declining birth rates",
            "Image": "https://d3ku2up3znex6l.cloudfront.net/public/sitereview_listing/76/61/01/15f16_c21e.jpg?c=308d",
            "URL": "https://syncni.com/view/5687/data-an-in-depth-look-at-ulster-s-declining-birth-rates"
          }],
          "IE-C" : [
            {
              "Title": "Data: An in-depth look at Connacht's declining birth rates",
              "Image": "https://d3ku2up3znex6l.cloudfront.net/public/sitereview_listing/b3/61/01/15f53_fb11.jpg?c=01af",
              "URL": "https://syncni.com/view/5696/data-an-in-depth-look-at-connacht-s-declining-birth-rates"
            }],
            "IE-L" : [
              {
                "Title": "Birth rates in Leinster and Dublin are falling. So what?",
                "Image": "https://www.digitaltimes.ie/wp-content/uploads/2021/02/birth-rates-in-dublin-too-low.jpg",
                "URL": "https://www.digitaltimes.ie/birth-rates-in-leinster-and-dublin-are-falling-so-what/"
              }],
              "IE-M" : [
                {
                  "Title": "Munster",
                  "Image":  "https://www.digitaltimes.ie/wp-content/uploads/2021/02/Irelands-birth-rate.jpg",
                  "URL": "https://www.digitaltimes.ie/irelands-birth-rate-is-decreasing-especially-in-munster/"
                }]
        });
    },
    loadCodes: function ({ commit }) {
      return d3.csv("countries-ga-url.csv").then(function(ga_codes) {
        var gaCodeMap = ga_codes.reduce(function (gaCodeMap, code) {
          gaCodeMap[code['ISO3166-2-Alpha-4']] = code;

          return gaCodeMap;
        }, {});

        return d3.csv("country-codes.csv").then(function(codes) {
          var codeMap = codes.reduce(function (codeMap, code) {
            var cc = code['ISO3166-2-Alpha-4'];
            codeMap[cc] = code;
            if (cc in gaCodeMap) {
              codeMap[cc].dfaie = gaCodeMap[cc];
            }
            return codeMap;
          }, {});
          commit('setCodes', codeMap);
          return codeMap;
        })
      })
    },
    loadData: function ({ commit, state, dispatch }) {
      return d3.json('nodes.geojson').then(function(nodes) {
        return d3.csv('links.csv').then(function(links) {
          commit('setData', { nodes, links })

          var fillData = function (codes) {
            var matchingLinks = links.filter(
              (link) => link.target === 'IE'
            ).reduce((states, link) => {
              states[link.source] = link.flow;
              return states;
            }, {});

            var states = {};

            states = nodes.features.reduce((states, node) => {
              if (node.id in codes && node.id in matchingLinks) {
                 states[node.id] = {
                   code: codes[node.id],
                   node: node,
                   flow: matchingLinks[node.id]
                 };
              }
              return states;
            }, states);

            commit('setStates', states);

            return states;
          };

          if (state.codes) {
            fillData(state.codes);
          } else {
            return dispatch('loadCodes').then(fillData);
          }

          return { nodes, links }
        })
      })
    }
  },
  getters: {
    statesByFlow: function (state) {
      var filter = state.filter;
      var states = state.states;
      var filteredStates = Object.keys(states).filter(function (ste) {
        if (ste in state.states) {
          for (var key in filter) {
            if (states[ste].code[key] !== filter[key]) {
              return false;
            }
          }
          return true;
        }
      });

      return filteredStates.reduce(function (list, ste) {
        list.push([ste, states[ste].flow]);
        return list;
      }, []).sort(function (a, b) {
        return b[1] - a[1];
      }).map(p => p[0]);
    },
    hasSelectedState: function (state) {
      return state.ste.length && state.isSelected
    },
    steCode: function (state) {
      if (state.ste.length && state.codes) {
        return state.codes[state.ste.toUpperCase()]
      }
      return null
    },
    steItem: function (state) {
      return ste => {
        if (ste.length) {
          return state.codes[ste.toUpperCase()]
        }
        return null
      }
    },
    steArticles: function (state) {
      if (state.ste.length && state.ste in state.articles) {
        return state.articles[state.ste]
      }
      return []
    },
    totalPopulation: function (state) {
      if (state.ste && state.states[state.ste]) {
        return state.states[state.ste].flow;
      }
    }
  }
})

export default store
